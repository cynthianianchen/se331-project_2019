import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherConfirmationProgressComponent } from './teacher-confirmation-progress.component';

describe('TeacherConfirmationProgressComponent', () => {
  let component: TeacherConfirmationProgressComponent;
  let fixture: ComponentFixture<TeacherConfirmationProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherConfirmationProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherConfirmationProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
